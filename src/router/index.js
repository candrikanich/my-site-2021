import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: () => import(/* webpackChunkName: "home" */ "@/views/Home.vue")
  },
  {
    path: "/home",
    name: "Home",
    component: () => import(/* webpackChunkName: "home" */ "@/views/Home.vue")
  },
  {
    path: "/mystory",
    name: "My Story",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "mystory" */ "@/views/Story.vue")
  },
  {
    path: "/mywork",
    name: "My Work",

    component: () => import(/* webpackChunkName: "mywork" */ "@/views/Work.vue")
  },
  {
    path: "/mycontact",
    name: "My Contact",

    component: () =>
      import(/* webpackChunkName: "mycontact" */ "@/views/Contact.vue")
  }
];

const router = new VueRouter({
  routes
});

export default router;
