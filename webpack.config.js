const GoogleFontsPlugin = require("google-fonts-webpack-plugin");

module.exports = {
  entry: "index.js",
  /* ... */
  plugins: [
    new GoogleFontsPlugin({
      fonts: [
        { family: "Newsreader", variants: ["300", "400", "700"] },
        { family: "Montserrat", variants: ["100", "400", "700", "900"] }
      ]
    })
  ]
};
