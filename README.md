# ChrisAndrikanich.com (Gitlab: my-site-2021)
[![pipeline status](https://gitlab.com/candrikanich/my-site-2021/badges/master/pipeline.svg)](https://gitlab.com/candrikanich/my-site-2021/-/commits/master)

[![coverage report](https://gitlab.com/candrikanich/my-site-2021/badges/master/coverage.svg)](https://gitlab.com/candrikanich/my-site-2021/-/commits/master)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### Gitlab CI/CD to Netlify Setup
See [Deploying a Vue.js app to Netlify using GitLab’s CI/CD pipeline](https://medium.com/js-dojo/deploying-vue-js-to-netlify-using-gitlab-continuous-integration-pipeline-1529a2bbf170#8ada)